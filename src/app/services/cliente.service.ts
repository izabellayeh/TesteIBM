import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  clienteUrl = 'https://dummy-blue-hunter.mybluemix.net/user/by-name';

  constructor(private http: HttpClient) { }

  getClientByName(name: string): Observable<any> {
    const url = `${this.clienteUrl}/${name}`;
    return this.http.get<any>(url).pipe(
      catchError(this.handleError<any>(`getHero id=${name}`))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
