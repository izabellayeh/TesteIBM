import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {

  private livroUrl = 'https://dummy-blue-hunter.mybluemix.net/book';

  constructor(private http: HttpClient) { }

  getAllLivros(): Observable<any> {
    const url = `${this.livroUrl}`;
    return this.http.get<any>(url).pipe(
      catchError(this.handleError<any>(`getHero id=${name}`))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
}
