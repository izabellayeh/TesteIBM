import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClienteService } from '../services/cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  cliente: any;
  formC: FormGroup;

  constructor(private fb: FormBuilder, private clienteService: ClienteService) {
    this.createFormC();
  }

  createFormC() {
    this.formC = this.fb.group({
      fullName: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  // declarar funcao getAllUsers()
  // dentro dela tem que chamar a funcao getallUser do servico
  getClientByName() {
    const fullName = this.formC.get('fullName').value.trim();
    this.clienteService.getClientByName(fullName)
      .subscribe(rest => {
        this.cliente = rest;
      });
  }

}

