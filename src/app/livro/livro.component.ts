import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LivrosService } from '../services/livros.service';
import { Title } from '@angular/platform-browser';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-livro',
  templateUrl: './livro.component.html',
  styleUrls: ['./livro.component.css']
})
export class LivroComponent implements OnInit {
  livros: any = [];
  filterLivros: any = [];

  formL: FormGroup;

  constructor(private fb: FormBuilder, private livrosService: LivrosService) {
    this.createForm();
  }
  createForm() {
    this.formL = this.fb.group({
      title: ['', Validators.required]
    });
  }
  ngOnInit() {
    this.getAllLivros();
  }

  getAllLivros() {
    this.livrosService.getAllLivros()
      .subscribe(rest => {
        this.livros = rest;
        this.getLivroByName();
      });
  }

  getLivroByName() {
    const title = this.formL.get('title').value.trim();
    if (title !== '') {
      this.filterLivros = [];
      this.livros.filter((livro) => {
        if (livro.title.toLowerCase().includes(title)) {
          this.filterLivros.push(livro);
        }
      });
    } else {
      this.filterLivros = this.livros.slice();
    }
  }

}
