import { Routes, RouterModule } from '@angular/router';
import { ClienteComponent } from './cliente/cliente.component';
import { LivroComponent } from './livro/livro.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  // Home
  {
    path: '',
    component: HomeComponent
  },
  // Cliente
  {
    path: '/Cliente',
    component: ClienteComponent
  },
  // Livros
  {
    path: '/Livro',
    component: LivroComponent
  }
];
export const RoutingModule = RouterModule.forRoot(routes);
